from tkinter import filedialog as fd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

if __name__ == '__main__':
    train_fp = fd.askdirectory()
    test_fp = fd.askdirectory()
    img_height = 28
    img_width = 28
    print(train_fp)
    print(test_fp)
    train = tf.keras.preprocessing.image_dataset_from_directory(
        train_fp, labels='inferred',
        label_mode='int',
        color_mode='rgb', batch_size=32, image_size=(img_height, img_width), shuffle=True, seed=1337, validation_split=None, subset=None,
    )
    test = tf.keras.preprocessing.image_dataset_from_directory(
        test_fp, labels='inferred',
        label_mode='int',
        color_mode='rgb', batch_size=32, image_size=(img_height, img_width), shuffle=True, seed=1337, validation_split=None, subset=None,
    )

    batch_size = 32
    epochs = 200
    AUTOTUNE = tf.data.AUTOTUNE
    train = train.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    test = test.cache().prefetch(buffer_size=AUTOTUNE)

    model = Sequential([
        layers.experimental.preprocessing.RandomFlip("horizontal",
                                                     input_shape=(img_height,
                                                                  img_width,
                                                                  3)),
        layers.experimental.preprocessing.RandomRotation(0.1),
        layers.experimental.preprocessing.RandomZoom(0.1),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.BatchNormalization(),
        layers.Dense(10)
    ])
    model.summary()
    model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  optimizer="adam", metrics=['accuracy'])
    history = model.fit(train, epochs=epochs, verbose=2,
                        validation_data=test)

    acc = model.evaluate(test, verbose=1)
    print(acc)

    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
