# Term_Project (手寫數字辨識)
---
## 訓練結果
![](Figure_1.png)
* val_accuracy大約96%
* val_loss約0.2425

## 程式碼解說

1. 引入所需套件，本次使用tensorflow來進行機器學習，matplotlib則是用來繪製結果圖表，tkinter是用來彈出選擇資料夾的dialog用的
```py
from tkinter import filedialog as fd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
```
2. 分別跳出兩個視窗選擇訓練與測試用的資料集
```py
train_fp = fd.askdirectory()
test_fp = fd.askdirectory()
```
3. 定義整個檔案中的圖片大小
```py
img_height = 28
img_width = 28
```
4. 利用tensorflow提供的`image_dataset_from_directory` function分別將訓練及測試資料以每個圖片所在的subfolder為label組合成dataset，同時會將大小不是28x28的圖片縮放成28x28，並且組合成dataset時圖片的順序會被隨機排列
```py
train = tf.keras.preprocessing.image_dataset_from_directory(
        train_fp, labels='inferred',
        label_mode='int',
        color_mode='rgb', batch_size=32, image_size=(img_height, img_width), shuffle=True, seed=1337, validation_split=None, subset=None,
    )
test = tf.keras.preprocessing.image_dataset_from_directory(
        test_fp, labels='inferred',
        label_mode='int',
        color_mode='rgb', batch_size=32, image_size=(img_height, img_width), shuffle=True, seed=1337, validation_split=None, subset=None,
    )
```
5. 設定機器學習的batch size跟epoch次數
```py
batch_size = 32
epochs = 200
```
6. 預載圖片提升效能
```py
AUTOTUNE = tf.data.AUTOTUNE
train = train.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
test = test.cache().prefetch(buffer_size=AUTOTUNE)
```
7. 模型設計
```py
model = Sequential([
        layers.experimental.preprocessing.RandomFlip("horizontal",
                                                     input_shape=(img_height,
                                                                  img_width,
                                                                  3)),
        layers.experimental.preprocessing.RandomRotation(0.1),
        layers.experimental.preprocessing.RandomZoom(0.1),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.BatchNormalization(),
        layers.Dense(10)
    ])
```
* 通過隨機翻轉、縮放、旋轉圖片來克服圖片資料及過小容易造成overfit的問題
```py
layers.experimental.preprocessing.RandomFlip("horizontal",
                                                     input_shape=(img_height,
                                                                  img_width,
                                                                  3)),
        layers.experimental.preprocessing.RandomRotation(0.1),
        layers.experimental.preprocessing.RandomZoom(0.1),
```
* Batch Normalization `layers.BatchNormalization()`
BN主要是在進行mini-batch training的時候，依各個batch來進行normalization (x - batch mean / stdv(batch))。其優點像是收斂速度較快、不需tune參數、降低overfiting以及減少gradient exploding or vanishing gradient。目前被部分的人認為比dropout好
8.  檢視所有在模型的layer
```py
model.summary()
```
9. 完成模型
```py
model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  optimizer="adam", metrics=['accuracy'])
```
10. 訓練模型
```py
history = model.fit(train, epochs=epochs, verbose=2,
                        validation_data=test)
```
11. 用test資料驗證
```py
acc = model.evaluate(test, verbose=1)
print(acc)
```
12. 用matplotlib顯示出準確率和loss的圖表
```py
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
```